package com.lightsource.monitoring.common

import org.joda.time.LocalDate
import org.joda.time.DateTime
import com.mongodb.DBObject
import com.lightsource.monitoring.TimeSeries

trait MonitoringStorage {
  
  def allknownMeterids(): List[String]
  def mostrecentEntryFor(meterid: String): Option[DBObject]
  def oldestEntryFor(meterid: String): Option[DBObject]
  def numberOfEntriesForInInterval(meterid: String, from: DateTime, to: DateTime): Long

  def hasOldOngoingAlarm(meterid: String, date: LocalDate): Boolean
  def findInstallationNameForMeter(meterid: String): Option[Object]

  def createAlarm(meterid: String, from: DateTime): Unit
  def closeAlarm(meterid: String, to: DateTime): Unit

  val storeFullDay: TimeSeries => Option[TimeSeries]
	val storeShowing: TimeSeries => Option[Int]

  //============derived timeseries==============  

  def entriesForInInterval(meterid: String, from: DateTime, to: DateTime): List[DBObject]
  def findPostProcessingTimeseries(meterid: String): String
  def getRateForTimeAndMeterId(dt: DateTime, meterid: String): Option[Double]

}