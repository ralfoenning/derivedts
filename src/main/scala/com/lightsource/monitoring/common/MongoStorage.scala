package com.lightsource.monitoring.common

import java.net.URLEncoder

import scala.io.Source

import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClients
import org.json4s.JsonAST.JObject
import org.json4s.native.JsonMethods.compact
import org.json4s.native.JsonMethods.parse
import org.json4s.native.JsonMethods.render
import org.json4s.string2JsonInput

import com.mongodb.casbah.MongoClient

import grizzled.slf4j.Logging

import org.joda.time.DateTime
import org.joda.time.LocalDate
import org.joda.time.DateTimeZone
import org.joda.time.LocalTime

import com.mongodb.casbah.commons.conversions.scala.RegisterJodaTimeConversionHelpers
import com.mongodb.casbah.commons.{ Imports, MongoDBObject, MongoDBList }
import com.mongodb.casbah.Imports.$set
import com.mongodb.casbah.Imports.wrapDBObj
import com.mongodb.casbah.commons.conversions.scala.RegisterJodaLocalDateTimeConversionHelpers
import org.json4s.native.JsonParser

import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.native.Json._
import org.json4s.native.JsonMethods._

import com.mongodb.casbah.MongoCollection
import com.mongodb.DuplicateKeyException
import com.mongodb.DBObject
import com.mongodb.WriteResult
import com.mongodb.WriteConcern

import java.util.Date

import com.lightsource.monitoring.TimeSeries

import org.joda.time.LocalDateTime

import com.lightsource.monitoring.hasGetCollectionMethod
import com.lightsource.monitoring.common.MongoStorage;

import scala.collection.JavaConversions._

class MongoStorage(val meterdb: hasGetCollectionMethod) extends Logging with MonitoringStorage {

  def allknownMeterids() = meterdb.getCollection("readings").distinct("meterId").toArray().toList.asInstanceOf[List[String]]

  def findOneReading(query: Imports.DBObject, fields: Imports.DBObject, orderBy: Imports.DBObject): Option[DBObject] = {
    val res = meterdb.getCollection("readings").findOne(query, fields, orderBy)
    if (res == null) None else Some(res)
  }
  protected def countReadings(template: DBObject) = meterdb.getCollection("readings").count(template).toLong

  final def mostrecentEntryFor(meterid: String) = {
    val query = MongoDBObject("meterId" -> meterid)
    val fields = MongoDBObject( /*"timestamp" -> 1*/ )
    val orderBy = MongoDBObject("timestamp" -> -1) //descending
    findOneReading(query, fields, orderBy)
  }

  final def oldestEntryFor(meterid: String) = {
    val query = MongoDBObject("meterId" -> meterid)
    val fields = MongoDBObject( /*"timestamp" -> 1*/ )
    val orderBy = MongoDBObject("timestamp" -> 1) //ascending
    findOneReading(query, fields, orderBy)
  }

  final def numberOfEntriesForInInterval(meterid: String, from: DateTime, to: DateTime) = {

    import com.mongodb.casbah.Imports._
    countReadings(("timestamp" $lte to $gte from) ++ ("meterId" -> meterid))
  }

  private val alarmcoll = meterdb.getCollection("alarms")

  def hasOldOngoingAlarm(meterid: String, date: LocalDate) = {
    import com.mongodb.casbah.Imports._
    RegisterJodaTimeConversionHelpers()

    val res = alarmcoll.find("meterId" $eq meterid).toArray().toList
    res foreach println
    val dtForCasbah = date.toDateTime(LocalTime.MIDNIGHT, DateTimeZone.UTC)
    alarmcoll.count(("from" $lte dtForCasbah) ++ ("meterId" -> meterid) ++ ("to" -> null)) > 0
  }

  def findInstallationNameForMeter(meterid: String) = {

    val lscrmdb = MongoClient("localhost", 27017)("lscrmdb")
    val installationscoll = lscrmdb("customerInstallations")
    val installationWhereMetersListContainsMeterid = installationscoll.find(MongoDBObject()).
      find { each =>
        {
          each.getAs[MongoDBList]("installationMeters").map { eachList => eachList.contains(meterid) }.getOrElse(false)
        }
      }
    installationWhereMetersListContainsMeterid.map { eachdbo => eachdbo.get("installationDescription") }
  }

  protected val makeAlarmObject = (meterid: String, from: DateTime, installationName: Option[Object]) => MongoDBObject("_id" -> (meterid + "-" + from)) ++
    ("installation" -> installationName) ++
    ("description" -> "No Production") ++
    ("meterId" -> meterid) ++
    ("from" -> from) ++
    ("to" -> null)

  def createAlarm(meterid: String, from: DateTime) = {
    val installationName = findInstallationNameForMeter(meterid)
    println("inserting " + meterid + "-" + from + " - " + installationName)
    val result = alarmcoll.insert(makeAlarmObject(meterid, from, installationName))

    //<<<<>>>>result.toString()
  }
  def closeAlarm(meterid: String, to: DateTime) = {
    println("closing " + meterid + "-" + to)
    val result = alarmcoll.update(MongoDBObject("meterId" -> meterid) ++ ("to" -> null), $set("to" -> to))

    //<<<<>>>>result.toString()
  }

  RegisterJodaTimeConversionHelpers()

  /*protected[mongo]*/ val produceObjectTimeSeries = (ts: TimeSeries) => {

    val (whatTheValuesMean, meterId, date, iso8601Times, values, showing, unit) = ts.tupled

    val createMongoOjbFrom = (time: DateTime, meterValue: Double) => {
      MongoDBObject("type" -> whatTheValuesMean) ++
        ("meterId" -> meterId) ++
        ("timestamp" -> time) ++
        ("value" -> meterValue) ++
        ("unit" -> unit)              //or         ("unit" -> s.unit.getOrElse("n/a")) as in cloudclient,     or not all 
    }

    val output = iso8601Times.zip(values).map(createMongoOjbFrom.tupled(_))

    output
  }

  val storeFullDay: TimeSeries => Option[TimeSeries] = ts => {

    val t = produceObjectTimeSeries(ts)

    val res = t.map { x => storeMeterValue(x, "readings") }

    logger.info(res)

    storeShowing(ts)

    val wasNew = res.exists { (200 to 299).contains(_) }

    if (wasNew) storeDayTotals(ts)

    if (wasNew) Some(ts) else None

  }

  def storeDayTotals(ts: TimeSeries) {

    val dayPartitions = ts.times.grouped(48).toList.zip(ts.values.grouped(48).toList)
    dayPartitions.foreach(dtval => storeDayTotal(ts.meaning, ts.mid, ts.date, dtval._1, dtval._2))
  }

  val storeDayTotal: (String, String, LocalDate, List[DateTime], List[Double]) => Option[(String, String, LocalDate, List[DateTime], List[Double])] = (t, mid, c, d, v) => {

    val sum = v.foldLeft(0.0)(_ + _)

    val daytotal =
      MongoDBObject("_id" -> s"""${mid.trim()}-${d(0).toString()}-${t}""") ++
        ("type" -> ("dayTotal-" + t)) ++
        ("meterId" -> mid) ++
        ("timestamp" -> d(0)) ++
        ("value" -> sum)

    storeMeterValue(daytotal, "totals")

    None
  }

  def storeMeterValue(dbo: DBObject, collection: String) = {

    val meterpart = dbo.getAs[String]("meterId").get.trim()
    val timestamppart = dbo.get("timestamp")
    val typepart = dbo.getAs[String]("type").get
    val id = MongoDBObject("_id" -> s"""${meterpart}-${timestamppart}-${typepart}""")

    writeAsCreateOnly(dbo ++ id, collection)
  }

  def writeAsCreateOnly(dbo: DBObject, collection: String) = {
    logger.info(dbo)
    try {
      val result = meterdb.getCollection(collection).insert(dbo)
      201
    } catch { case e: DuplicateKeyException => 409 }
  }

  val storeShowing: TimeSeries => Option[Int] = ts => {

    ts match {
      case TimeSeries(mean @ _, mid @ _, _, times @ _, _, Some(showingVal), _, _, _) => {
        val obj = MongoDBObject("type" -> (mean + "-showing")) ++
          ("meterId" -> (mid + "-showing")) ++
          ("timestamp" -> times(0)) ++
          ("value" -> showingVal)

        Some(storeMeterValue(obj, "readings"))
      }
      case _ => None
    }
  }

  //============methods for derived timeseries==============

  def entriesForInInterval(meterid: String, from: DateTime, to: DateTime): List[DBObject] = {
import com.mongodb.casbah.Imports._
    findReadings(("timestamp" $lte to $gte from) ++ ("meterId" -> meterid))
  }

  protected def findReadings(template: DBObject): List[DBObject] = {

    println(template)

    meterdb.getCollection("readings").find(template).limit(5000).toArray().toList
  }

  def findPostProcessingTimeseries(meterid: String) = {

    Map(
      "14172978" -> ("2394000121232", "consumption"),
      "14172974" -> ("2394000121241", "consumption"))

    Map(
      "14172978" -> "2394000121232",
      "14172974" -> "2394000121241",
      "2394000121241" -> "14172974",
      "2394000121232" -> "14172978").get(meterid).get
  }

  def getRateForTimeAndMeterId(dt: DateTime, meterid: String) = Some(0.137)

}

object MongoStorage extends MonitoringStorage {

  val theStorage = new MongoStorage(MongoClient("localhost", 27017)("meterdb"))

  override def allknownMeterids() = theStorage.allknownMeterids()
  override def mostrecentEntryFor(meterid: String) = theStorage.mostrecentEntryFor(meterid)
  override def oldestEntryFor(meterid: String) = theStorage.oldestEntryFor(meterid)
  override def numberOfEntriesForInInterval(meterid: String, from: DateTime, to: DateTime): Long = theStorage.numberOfEntriesForInInterval(meterid, from, to)

  override def hasOldOngoingAlarm(meterid: String, date: LocalDate) = theStorage.hasOldOngoingAlarm(meterid, date)

  override def createAlarm(meterid: String, from: DateTime) = theStorage.createAlarm(meterid, from)

  override def findInstallationNameForMeter(meterid: String) = theStorage.findInstallationNameForMeter(meterid)

  override def closeAlarm(meterid: String, to: DateTime) = theStorage.closeAlarm(meterid, to)

  override val storeFullDay = theStorage.storeFullDay

  override val storeShowing: TimeSeries => Option[Int] = theStorage.storeShowing

  override def entriesForInInterval(meterid: String, from: DateTime, to: DateTime): List[DBObject] = theStorage.entriesForInInterval(meterid, from, to)

  override def findPostProcessingTimeseries(meterid: String): String = theStorage.findPostProcessingTimeseries(meterid)

  def getRateForTimeAndMeterId(dt: DateTime, meterid: String): Option[Double] = theStorage.getRateForTimeAndMeterId(dt, meterid)

}