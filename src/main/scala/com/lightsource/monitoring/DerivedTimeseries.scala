package com.lightsource.monitoring

trait DerivedTimeseries {

  val createOtherTimeseriesFrom: TimeSeries => List[TimeSeries]
}