package com.lightsource.monitoring

import grizzled.slf4j.Logging
import com.mongodb.BasicDBObject
import com.mongodb.casbah.commons.MongoDBObject
import com.mongodb.casbah.Imports._
import java.util.Date
import org.joda.time.DateTime
import java.lang.Math._

class DeltaTimeseries3(rep: hasGetCollectionMethod) extends DerivedTimeseries with Logging {

  implicit def inttoint(i: Int) = new { def Entries = i }

  val createOtherTimeseriesFrom: TimeSeries => List[TimeSeries] = ots => {
    require(ots.cumulativevalues.size == 1)

    val newcumulative = ots.cumulativevalues.head.toDouble

    logger.info(minimumIntervalOverLast(ots.mid, 6))

    val prevcumulative = lastCumulativeOfSinceOrNone(ots, 3 * minimumIntervalOverLast(ots.mid, 6 Entries))

    logger.info(prevcumulative)
    val s = ots.copy(
        mid = ots.mid+"-delta",
        meaning = "generation",
        values = List(if (prevcumulative.isDefined) newcumulative - prevcumulative.get else -1))
    List(s)
  }

  val createfrom = (ots: TimeSeries) => {
    
    val s = createOtherTimeseriesFrom(ots).head

    rep.getCollection("readings").insert(MongoDBObject("type" -> s.meaning) ++
      ("meterId" -> s.mid) ++
      ("timestamp" -> s.times.head) ++
      ("value" -> s.values.head) ++
      ("cumulativevalue" -> s.cumulativevalues.head.toDouble))
  }

  val lastCumulativeOfSinceOrNone = (timeseries: TimeSeries, minutes: Int) => {
    val dbo = lastInDbSince(timeseries.mid+"-delta", timeseries.times.head.minusMinutes(minutes))
    (if (dbo == null) None else Some(dbo)).
      map(_.get("cumulativevalue").asInstanceOf[Double])
  }

  val minimumIntervalOverLast = (mid: String, n: Int) =>
    last(mid, n).::(100000000L).::(50000000L).sliding(2).foldLeft(Long.MaxValue)(((c, l) => min(abs(l.head - l.tail.head), c))) / 1000 / 60 toInt

  lazy val last = (mid: String, n: Int) => findlast(mid, n)

  val findlast = (mid: String, n: Int) => {
    import scala.collection.JavaConversions._
    rep.getCollection("readings").
      find(MongoDBObject("meterId" -> mid)).
      sort(MongoDBObject("timestamp" -> -1)).
      limit(n).
      toArray.toList.
      map(_.get("timestamp").asInstanceOf[Date]).
      map(dt => new DateTime(dt).getMillis)
  }

  private val latestDbo = (mid: String) => rep.getCollection("readings").findOne(MongoDBObject("meterId" -> mid), MongoDBObject(), MongoDBObject("timestamp" -> -1))
  private val lastInDbSince = (mid: String, t: DateTime) => {
    rep.getCollection("readings").findOne(MongoDBObject("meterId" -> mid) ++
      ("timestamp" $gt t),
      MongoDBObject(), MongoDBObject("timestamp" -> -1))
  }

}