package com.lightsource.monitoring

class NoopTimeseriesFunctions  extends DerivedTimeseries {
  override val createOtherTimeseriesFrom = (ts: TimeSeries) => List(ts)
}