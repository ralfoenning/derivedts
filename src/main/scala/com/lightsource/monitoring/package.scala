package com.lightsource

import com.mongodb.DBCollection
import com.lightsource.monitoring.common.MongoStorage
import com.mongodb.casbah.MongoClient
package object monitoring {

  val monitoringStorage = MongoStorage

  type hasGetCollectionMethod = {
    def getCollection(name: String): DBCollection
  }

  val whattocreate = Map(("generation-cumulative", "kwh") -> ("generation", "kwh"),
    ("generation", "kwh") -> ("consumption", "kwh"))

  type typeunitpair = Tuple2[String, String]

  val whattoderive = (o: typeunitpair) => o match {
    case ("generation-cumulative", u @ _) => ("generation", u)
    case ("export-cumulative", u @ _)     => ("export", u)
    case ("generation", u @ _)            => ("consumption", u)
    case ("export", u @ _)                => ("consumption", u)
    case ("consumption", "kwh")           => ("consumption", "pence")
  }
  
  val hasCollectionMethodThingi = MongoClient("localhost", 27017).getDB("meterdb")

  val howtoderive = (o: typeunitpair) => o match {
    case ("export", u @ _)      => new RealTimeseriesFunctions(monitoringStorage)
    case _ => new NoopTimeseriesFunctions
//    case ("generation-cumulative", u @ _) => new DeltaTimeseries(hasCollectionMethodThingi)
  }

}