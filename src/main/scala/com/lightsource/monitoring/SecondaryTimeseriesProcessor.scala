package com.lightsource.monitoring

import org.vertx.java.core.AsyncResult
import org.vertx.scala.core.eventbus.Message
import org.vertx.scala.platform.Verticle

import com.lightsource.monitoring.common.MongoStorage

import grizzled.slf4j.Logger

class SecondaryTimeseriesProcessor extends Verticle {

  val glogger = Logger[this.type]

  implicit val monitoringStorage = MongoStorage

  System.setProperty(org.slf4j.impl.SimpleLogger.DEFAULT_LOG_LEVEL_KEY, "DEBUG")
  System.setProperty(org.slf4j.impl.SimpleLogger.SHOW_DATE_TIME_KEY, "false")
  System.setProperty(org.slf4j.impl.SimpleLogger.SHOW_LOG_NAME_KEY, "false")
  System.setProperty(org.slf4j.impl.SimpleLogger.SHOW_THREAD_NAME_KEY, "false")

  override def start() = {

    vertx.eventBus.registerHandler("meterimport-feed", { message: Message[String] =>
      {
        val m = message.body()
        container.logger.info("Received timeseries: " + m)

        val t = TimeSeries.fromJson(m)
        container.logger.info("Reconstructed timeseries: " + m)
        
        

        val tfunc = howtoderive(t.meaning, "")
        val newtimeseriesseq = tfunc.createOtherTimeseriesFrom(t)
        newtimeseriesseq.map(monitoringStorage.storeFullDay(_))

      }
    }, { asyncResult: AsyncResult[Void] =>
      println("""The "meterimport-feed" handler has been registered across the cluster ok? """ + asyncResult.succeeded())
    })

    val config = container.config()
    container.logger().info(config.getString("foo"))
    container.logger().info("Derived creator verticle started")

  }

}