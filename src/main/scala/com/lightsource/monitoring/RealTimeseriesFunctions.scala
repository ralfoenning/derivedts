package com.lightsource.monitoring

import com.lightsource.monitoring.common.MonitoringStorage
import grizzled.slf4j.Logging

class RealTimeseriesFunctions(rep: MonitoringStorage) extends DerivedTimeseries with Logging {

  def logTimeFor[A](exp: String, f: => A): A = {
    val s = System.nanoTime
    logger.info(exp + "...")
    val ret = f
    logger.info(exp + ". Done in " + (System.nanoTime - s) / 1e6 + "ms")
    ret
  }

  val createOtherTimeseriesFrom: TimeSeries => List[TimeSeries] = thistimeseries => {

    val exporttimeseries = thistimeseries
    val exportmid = thistimeseries.mid
    val generationmid = rep.findPostProcessingTimeseries(exportmid)

    val subsettimesbyexportkwh = 
      logTimeFor("A", 
        (exporttimeseries.times.zip(exporttimeseries.values)).filterNot { t => rep.entriesForInInterval(generationmid, t._1, t._1).isEmpty } 
    )

    val subsettimesbyconsumptionkwh = 
            logTimeFor("B", 
subsettimesbyexportkwh.map { t =>
      {
        val dt = t._1
        val exportval = t._2

        val consumptionval = rep.entriesForInInterval(generationmid, dt, dt).map { other => other.get("value").asInstanceOf[Double] - exportval }.map { num => Math.abs(num) }.head
        (dt, consumptionval)
      }
    }
            )

    val consumptionkwhTs = TimeSeries("consumption kwh", generationmid + "-" + exporttimeseries.mid + "-kwh", 
        exporttimeseries.date, subsettimesbyexportkwh.map(_._1), subsettimesbyconsumptionkwh.map(_._2), None, Some("kwh"))

    //    val consumptionkwhTs2 = TimeSeries("consumption kwh", generationmid + "-" + exporttimeseries.mid + "-kwh", exporttimeseries.date, subsettimesbyconsumptionkwh.unzip._1, subsettimesbyconsumptionkwh.unzip._2)

    val subsettimesbyconsumptionpence = subsettimesbyconsumptionkwh.map { t =>
      {
        val dt = t._1
        val consumptionval = t._2

        val consumptionpence = rep.getRateForTimeAndMeterId(dt, generationmid).getOrElse(Double.NaN) * consumptionval
        (dt, consumptionpence)
      }
    }

    val consumptionpenceTs = TimeSeries("consumption pence", generationmid + "-" + exporttimeseries.mid + "-pence", 
        exporttimeseries.date, subsettimesbyexportkwh.map(_._1), subsettimesbyconsumptionpence.map(_._2), None, Some("pence"))

    consumptionkwhTs :: consumptionpenceTs :: Nil

  }

  //=============derived version 2===============

  trait Op[T] { def eval: T }
  trait NameOp extends Op[String]
  trait IdOp extends Op[String]
  trait ValueOp extends Op[Number]
  case class ConsumptionNameOp(args: String*) extends NameOp { def eval: String = "consumption" }
  case class ConsumptionIdOp(args: String*) extends IdOp { def eval: String = args.mkString("-") }
  case class GenerationMinusExportOp(genVal: Double, expVal: Double) extends ValueOp { def eval: Number = genVal - expVal }

  val createDerived: List[TimeSeries] => (NameOp, IdOp, ValueOp) => TimeSeries = tslist => (nop, iop, vop) => {

    tslist match {
      case List(ts1, ts2) => {

        def inboth[T](l1: List[T], l2: List[T]): List[T] = {
          val desc = List(l1, l2).sortBy(_.size)
          desc(0).diff(desc(0).diff(desc(1)))
        }

      }
    }

    null
  }
}