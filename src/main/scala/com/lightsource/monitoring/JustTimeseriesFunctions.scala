package com.lightsource.monitoring

import com.lightsource.monitoring.common.MonitoringStorage
import grizzled.slf4j.Logging

class JustTimeseriesFunctions(rep: MonitoringStorage) extends Logging {
  
  val createOthersFrom = (tslist : List[TimeSeries]) => {
    
    val otypes = tslist.map(ts => (ts.meaning, "any"))
    
    val targettypes = otypes map whattoderive
    
//    DeltaTimeseriesCreator.>>>>
    
  }

  val storeSingleMeterData: TimeSeries => Option[TimeSeries] = ts => rep.storeFullDay(ts)

  def logTimeFor[A](exp: String, f: => A): A = {
    val s = System.nanoTime
    logger.info(exp + "...")
    val ret = f
    logger.info(exp + ". Done in " + (System.nanoTime - s) / 1e6 + "ms")
    ret
  }

  //=============derived version 2===============

  trait Op[T] { def eval: T }
  trait NameOp extends Op[String]
  trait IdOp extends Op[String]
  trait ValueOp extends Op[Number]
  case class ConsumptionNameOp(args: String*) extends NameOp { def eval: String = "consumption" }
  case class ConsumptionIdOp(args: String*) extends IdOp { def eval: String = args.mkString("-") }
  case class GenerationMinusExportOp(genVal: Double, expVal: Double) extends ValueOp { def eval: Number = genVal - expVal }

  val createDerived: List[TimeSeries] => (NameOp, IdOp, ValueOp) => TimeSeries = tslist => (nop, iop, vop) => {

    tslist match {
      case List(ts1, ts2) => {

        def inboth[T](l1: List[T], l2: List[T]): List[T] = {
          val desc = List(l1, l2).sortBy(_.size)
          desc(0).diff(desc(0).diff(desc(1)))
        }

      }
    }

    null
  }
}