package com.lightsource.monitoring.common

import com.github.fakemongo.Fongo

import grizzled.slf4j.Logging

object FongoStorage extends MongoStorage(new Fongo("fakemongo").getDB("meterdb")) with Logging {

  override def findInstallationNameForMeter(meterid: String) = null
  
}