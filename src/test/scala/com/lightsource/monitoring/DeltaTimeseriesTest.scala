package com.lightsource.monitoring

import org.json4s.JsonDSL._
import org.json4s.native.Json._
import org.json4s.native.JsonMethods._
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import org.joda.time.DateTime
import org.joda.time.LocalDate
import com.github.fakemongo.Fongo
import com.mongodb.casbah.commons.MongoDBObject
import com.mongodb.casbah.commons.Imports._
import com.mongodb.casbah.WriteConcern
import scala.collection.JavaConversions._
import com.mongodb.casbah.commons.conversions.scala.RegisterJodaTimeConversionHelpers

@RunWith(classOf[JUnitRunner])
class DeltaTimeseriesTest extends Specification {

  "delta timeseries" should {
    val all = (rep: hasGetCollectionMethod) => rep.getCollection("readings").find(MongoDBObject(), MongoDBObject("_id" -> 0) ++ ("timestamp" -> 1) ++ ("value" -> 1) ++ ("cumulativevalue" -> 1)).toArray().toList

    "work" in {

      RegisterJodaTimeConversionHelpers()

      val hasCollectionMethodThingi = new Fongo("fakemongo").getDB("meterdb")
      hasCollectionMethodThingi.getCollection("readings").remove(MongoDBObject(), WriteConcern.Normal)

      val s = TimeSeries("generation-cumulative", "123", LocalDate.now(),
        List(DateTime.parse("2015-04-01T10:30:00.000")), List(), None, None,
        List(677.12))

      new DeltaTimeseries(hasCollectionMethodThingi).createfrom(s)

      all(hasCollectionMethodThingi) foreach println

      val s2 = s.copy(times = List(DateTime.parse("2015-04-01T10:45:00.000")), cumulativevalues = List(678.13))

      println("---")
      new DeltaTimeseries(hasCollectionMethodThingi).createfrom(s2)

      all(hasCollectionMethodThingi) foreach println

      val s3 = s.copy(times = List(DateTime.parse("2015-04-01T11:45:00.000")), cumulativevalues = List(679.14))

      println("---")
      new DeltaTimeseries(hasCollectionMethodThingi).createfrom(s3)

      all(hasCollectionMethodThingi) foreach println
      val s4 = s.copy(times = List(DateTime.parse("2015-04-01T12:00:00.000")), cumulativevalues = List(680.17))

      println("---")
      new DeltaTimeseries(hasCollectionMethodThingi).createfrom(s4)
      all(hasCollectionMethodThingi) foreach println

      println("---")
      new DeltaTimeseries(hasCollectionMethodThingi).createfrom(
        s.copy(times = List(DateTime.parse("2015-04-01T12:01:00.000")), cumulativevalues = List(680.37)))
      all(hasCollectionMethodThingi) foreach println
      println("---")
      new DeltaTimeseries(hasCollectionMethodThingi).createfrom(
        s.copy(times = List(DateTime.parse("2015-04-01T12:15:00.000")), cumulativevalues = List(681.44)))
      all(hasCollectionMethodThingi) foreach println
      println("---")
      new DeltaTimeseries(hasCollectionMethodThingi).createfrom(
        s.copy(times = List(DateTime.parse("2015-04-01T12:30:00.000")), cumulativevalues = List(682.55)))
      all(hasCollectionMethodThingi) foreach println
      println("---")
      new DeltaTimeseries(hasCollectionMethodThingi).createfrom(
        s.copy(times = List(DateTime.parse("2015-04-01T12:45:00.000")), cumulativevalues = List(683.66)))
      all(hasCollectionMethodThingi) foreach println
      println("---")
      new DeltaTimeseries(hasCollectionMethodThingi).createfrom(
        s.copy(times = List(DateTime.parse("2015-04-01T13:00:00.000")), cumulativevalues = List(684.77)))
      all(hasCollectionMethodThingi) foreach println
      println("---")
      new DeltaTimeseries(hasCollectionMethodThingi).createfrom(
        s.copy(times = List(DateTime.parse("2015-04-01T13:15:00.000")), cumulativevalues = List(685.88)))
      all(hasCollectionMethodThingi) foreach println
      println("---")
      new DeltaTimeseries(hasCollectionMethodThingi).createfrom(
        s.copy(times = List(DateTime.parse("2015-04-01T13:30:00.000")), cumulativevalues = List(686.99)))
      all(hasCollectionMethodThingi) foreach println
      
      all(hasCollectionMethodThingi).reverse.head.toString() must_==
        """{ "timestamp" : { "$date" : "2015-04-01T12:30:00.000Z"} , "value" : 1.1100000000000136 , "cumulativevalue" : 686.99}"""
      ok
    }
  }

}